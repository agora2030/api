'use strict'
const path = require('path')

module.exports = {
  env: 'test',
  jwtSecret: 'agora2030jwttest',
  uploadDir: path.join(__dirname, '../../src/test/uploads'),
  host: 'http://localhost:8080',
  staticUrl: 'http://localhost:3000/v1/static',
  allowedDomains: ['http://localhost:3000', 'http://0.0.0.0:8080', 'http://localhost:8080', 'http://localhost:8081', 'admin-agora2030.colabtech.org'],
  server: {
    host: 'localhost',
    port: 3000
  },
  nodemailer: {
    email: 'feirazikatest@gmail.com',
    password: 'fiotec2017'
  },
  sequelize: {
    databaseUrl: '', // Only production
    database: 'agora2030test',
    user: 'root',
    password: 'root',
    params: {
      host: 'localhost',
      dialect: 'mysql',
      logging: false,
      operatorsAliases: false,
      define: {
        charset: 'utf8mb4',
        freezeTableName: true,
        paranoid: true,
        defaultScope: {
          attributes: {exclude: ['deletedAt']}
        }
      }
    }
  }
}
