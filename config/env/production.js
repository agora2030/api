'use strict'

module.exports = {
  env: 'production',
  jwtSecret: process.env.JWT_SECRET,
  cipherPassword: process.env.CHIPER_SECRET,
  uploadDir: process.env.UPLOAD_DIR,
  apiUrl: process.env.API_URL,
  host: process.env.HOST,
  staticUrl: process.env.STATIC_URL,
  facebookAppToken: process.env.FACEBOOK_APP_TOKEN,
  oneSignalAppId: process.env.ONE_SIGNAL_APP_ID,
  oneSignalToken: process.env.ONE_SIGNAL_TOKEN,
  allowedDomains: process.env.ALLOWED_DOMAINS.split(','),
  server: {
    host: process.env.SERVER_HOST,
    port: process.env.SERVER_PORT
  },
  fromName: process.env.SMTP_FROM_NAME,
  siteEmail: process.env.SMTP_SITE_EMAIL,
  nodemailer: {
    service: 'fiocruzsmtp',
    type: 'SMTP',
    host: process.env.SMTP_HOST,
    port: 587,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS
    },
    tls: {
      rejectUnauthorized: false
    }
  },
  sequelize: {
    databaseUrl: process.env.DATABASE_URL,
    params: {
      host: process.env.DATABASE_HOST,
      dialect: 'mysql',
      logging: false,
      operatorsAliases: false,
      define: {
        charset: 'utf8mb4',
        freezeTableName: true,
        paranoid: true,
        defaultScope: {
          attributes: {exclude: ['deletedAt']}
        }
      }
    }
  }
}
