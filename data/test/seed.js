'use strict'

const models = require('../../src/models')
const fixtures = require('./data')

module.exports = {
  init
}

function init () {
  console.log('--> Load test seed')
  return models.sequelize.sync({force: true})
  .then(createUsers)
  .then(createOds)
  .then(createMissions)
  .then(createIniatives)
  .then(finish => {
    console.log('--> Test seed finished')
  })
  .catch(err => {
    console.log('Seed error: ', err)
  })
}

function createUsers () {
  return models.User.bulkCreate(fixtures.users, {individualHooks: true})
}

function createOds () {
  return models.Ods.bulkCreate(require('../util/ods'), {individualHooks: true})
}

function createMissions () {
  return models.Mission.bulkCreate(require('../util/missions'), {individualHooks: true})
}

async function createIniatives () {
  const missions = await models.Mission.findAndCountAll()
  const initiatives = fixtures.initiatives.map(item => {
    item.missionId = missions.rows[0].id
    return item
  })
  return models.Initiative.bulkCreate(initiatives)
}
