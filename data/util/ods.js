module.exports = [
  {
    number: 1,
    title: 'Erradicação da Pobreza',
    description: 'Acabar com a pobreza em todas as suas formas, em todos os lugares.',
    color: '#E3253C'
  },
  {
    number: 2,
    title: 'Fome Zero e Agricultura Sustentável',
    description: 'Acabar com a fome, alcançar a segurança alimentar e melhoria da nutrição e promover a agricultura sustentável.',
    color: '#DDA63A'
  },
  {
    number: 3,
    title: 'Saúde e Bem-Estar',
    description: 'Assegurar uma vida saudável e promover o bem-estar para todos, em todas as idades.',
    color: '#4D9F46'
  },
  {
    number: 4,
    title: 'Educação de Qualidade',
    description: 'Assegurar a educação inclusiva e equitativa de qualidade, e promover oportunidades de aprendizagem ao longo da vida para todos.',
    color: '#C4202E'
  },
  {
    number: 5,
    title: 'Igualdade de Gênero',
    description: 'Alcançar a igualdade de gênero e empoderar todas as mulheres e meninas.',
    color: '#EE412B'
  },
  {
    number: 6,
    title: 'Água Potável e Saneamento',
    description: 'Assegurar a disponibilidade e a gestão sustentável da água e saneamento para todos.',
    color: '#27BCE2'
  },
  {
    number: 7,
    title: 'Energia Acessível e Limpa',
    description: 'Assegurar o acesso confiável, sustentável, moderno e a preço acessível à energia para todos.',
    color: '#F9C314'
  },
  {
    number: 8,
    title: 'Trabalho Decente e Crescimento Econômico',
    description: 'Promover o crescimento econômico sustentado, inclusivo e sustentável, o emprego pleno e produtivo e o trabalho decente para todos.',
    color: '#A11C43'
  },
  {
    number: 9,
    title: 'Indústria, Inovação e Infraestrutura',
    description: 'Construir infraestruturas resilientes, promover a industrialização inclusiva e sustentável e fomentar a inovação.',
    color: '#F16A2B'
  },
  {
    number: 10,
    title: 'Redução da Desigualdades',
    description: 'Reduzir a desigualdade dentro dos países e entre eles.',
    color: '#DD1768'
  },
  {
    number: 11,
    title: 'Cidades e Comunidades Sustentáveis',
    description: 'Tornar as cidades e os assentamentos humanos inclusivos, seguros, resilientes e sustentáveis.',
    color: '#F89C28'
  },
  {
    number: 12,
    title: 'Consumo e Produção Responsáveis',
    description: 'Assegurar padrões de produção e de consumo sustentáveis.',
    color: '#BE8B2C'
  },
  {
    number: 13,
    title: 'Ação Contra a Mudança Global do Clima',
    description: 'Tomar medidas urgentes para combater a mudança do clima e seus impactos.',
    color: '#417F44'
  },
  {
    number: 14,
    title: 'Vida na Água',
    description: 'Conservar e promover o uso sustentável dos oceanos, dos mares e dos recursos marinhos para o desenvolvimento sustentável.',
    color: '#1995D3'
  },
  {
    number: 15,
    title: 'Vida Terrestre',
    description: 'Proteger, recuperar e promover o uso sustentável dos ecossistemas terrestres, gerir de forma sustentável as florestas, combater a desertificação, deter e reverter a degradação da terra e deter a perda.',
    color: '#5BB947'
  },
  {
    number: 16,
    title: 'Paz, Justiça e Instituições Eficazes',
    description: 'Promover sociedades pacíficas e inclusivas para o desenvolvimento sustentável, proporcionar o acesso à justiça para todos e construir instituições eficazes, responsáveis e inclusivas em todos os níveis.',
    color: '#00699D'
  },
  {
    number: 17,
    title: 'Parcerias e Meios de Implementação',
    description: 'Fortalecer os meios de implementação e revitalizar a parceria global para o desenvolvimento sustentável.',
    color: '#19486B'
  }
]
