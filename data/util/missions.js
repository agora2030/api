module.exports = [
  {
    title: 'Pessoas',
    description: 'Não há água potável, a vegetação é escassa e o terreno é árido. Sua missão é trazer as pessoas de volta, acabando com a fome e a pobreza e dando-lhes condições para retomarem suas casas e reconstruírem suas vidas.',
    order: 1,
    actionView: 2,
    actionLike: 1
  },
  {
    title: 'Planeta',
    description: 'O povoado Ágora precisa de saneamento básico, energia limpa, aterro sanitário e tratamento de água. Sua missão é fazer com que os cidadãos do povoado voltem a conviver em harmonia com a natureza.',
    order: 2,
    actionView: 5,
    actionFavorite: 1,
    actionShare: 1,
    actionLike: 3
  },
  {
    title: 'Prosperidade',
    description: 'O povoado Ágora precisa agora investir em dignidade para os seus moradores. Sua missão é promover a capacidade de produzir uma vida próspera, baseada em lógicas solidárias e sustentáveis de produção e consumo.',
    order: 3,
    actionView: 10,
    actionFavorite: 3,
    actionShare: 3,
    actionLike: 7,
    actionComment: 2
  },
  {
    title: 'Parceria',
    description: 'A sustentabilidade do povoado Ágora só será possível se todos trabalharem juntos, no coletivo! Sua missão é promover parcerias entre a sociedade civil, empresas, governos e instituições em geral.',
    order: 4,
    actionView: 15,
    actionFavorite: 7,
    actionShare: 7,
    actionLike: 10,
    actionComment: 4,
    actionDonate: 1
  },
  {
    title: 'Paz',
    description: 'Para que o povoado Ágora seja sustentável é preciso promover a paz e a harmonia entre os cidadãos. É preciso investir em educação para respeito das diferenças, igualdade, inclusão e justiça.',
    order: 5,
    actionView: 25,
    actionFavorite: 10,
    actionShare: 10,
    actionLike: 15,
    actionComment: 7,
    actionDonate: 2
  },
  {
    title: 'Curadoria',
    description: 'Agora que o povoado Ágora foi reconstruído e as pessoas voltaram a viver em harmonia com a natureza e outros povos, você precisa completar a última missão do desafio para ganhar o seu certificado de curador.',
    order: 6,
    actionView: 40,
    actionFavorite: 15,
    actionShare: 15,
    actionLike: 20,
    actionComment: 10,
    actionDonate: 4
  }
]
