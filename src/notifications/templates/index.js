const fs = require('fs')
const files = fs.readdirSync(__dirname)

let templates = {}

files.forEach((file) => {
  if (!file.match(/.handlebars/i)) {
    return
  }
  let name = file.replace(/.handlebars/gi, '')
  templates[name] = fs.readFileSync(`${__dirname}/${file}`, 'utf8')
})

module.exports = templates
