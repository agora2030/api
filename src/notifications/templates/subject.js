module.exports = {
  EmailConfirmacao: 'Confirmação de Email',
  EmailBoasVindas: 'Seja bem-vindo ao Desafio Ágora 2030!',
  RecuperarSenha: 'Recuperar Senha',
  MissaoPessoas: 'Novo selo na área: Pessoas!',
  MissaoPlaneta: 'Novo selo na área: Planeta!',
  MissaoProsperidade: 'Novo selo na área: Prosperidade!',
  MissaoParceria: 'Novo selo na área: Parceria!',
  MissaoPaz: 'Novo selo na área: Paz!',
  MissaoCuradoria: '[Certificado] Agora você é um Curador do Desafio Ágora 2030!'
}
