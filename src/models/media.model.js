'use strict'

const config = require('../../config/env')

module.exports = (sequelize, DataTypes) => {
  const Media = sequelize.define('Media', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    originalFileName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    extName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    contentType: {
      type: DataTypes.STRING,
      allowNull: false
    },

    mediaType: {
      type: DataTypes.ENUM('image', 'file'),
      allowNull: false
    },

    fileName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    folderPath: {
      type: DataTypes.STRING,
      allowNull: false
    },

    caption: {
      type: DataTypes.STRING
    }
  }, {
    tableName: 'medias',
    paranoid: false
  })

  Media.associate = (models) => {
    Media.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', allowNull: false}})
  }

  Media.prototype.toJSON = function () {
    let values = Object.assign({}, this.get())
    const folder = config.staticUrl + values.folderPath

    if (values.mediaType === 'file') {
      values.url = `${folder}${values.fileName}${values.extName}`
    } else if (values.mediaType === 'image') {
      values.urls = {}
      values.urls.original = `${folder}original${values.extName}`
      config.imageStyles.forEach(style => {
        values.urls[style.name] = `${folder}${style.name}${values.extName}`
      })
    }

    delete values.extName
    delete values.contentType
    delete values.fileName
    delete values.folderPath
    delete values.updatedAt
    delete values.createdAt

    return values
  }

  return Media
}
