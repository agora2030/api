'use strict'

module.exports = (sequelize, DataTypes) => {
  const Url = sequelize.define('Url', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    url: {
      type: DataTypes.STRING,
      allowNull: false
    },

    title: {
      type: DataTypes.STRING
    },

    type: {
      type: DataTypes.ENUM('audio', 'video', 'link')
    }

  }, {
    tableName: 'urls',
    paranoid: false
  })

  Url.associate = (models) => {
    Url.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', allowNull: false}})
  }

  return Url
}
