'use strict'

module.exports = (sequelize, DataTypes) => {
  const Ods = sequelize.define('Ods', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    number: {
      type: DataTypes.INTEGER,
      allowNull: false
    },

    title: {
      type: DataTypes.STRING,
      allowNull: false
    },

    description: {
      type: DataTypes.STRING,
      allowNull: false
    },

    color: {
      type: DataTypes.STRING,
      allowNull: false
    },

    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }

  }, {
    tableName: 'ods'
  })

  Ods.associate = (models) => {
    Ods.belongsToMany(models.Initiative, {as: 'initiatives', through: models.InitiativeOds, foreignKey: 'odsId'})
  }

  return Ods
}
