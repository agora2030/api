'use strict'

module.exports = (sequelize, DataTypes) => {
  const Trace = sequelize.define('Trace', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    type: {
      type: DataTypes.ENUM('actionLike', 'actionView', 'actionShare', 'actionDonate', 'actionFavorite', 'actionComment'),
      allowNull: false
    }

  }, {
    tableName: 'traces',
    paranoid: false
  })

  Trace.associate = (models) => {
    Trace.belongsTo(models.User, {as: 'user', foreignKey: {field: 'userId', allowNull: false}})
    Trace.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', allowNull: false}})
  }

  Trace.prototype.toJSON = function () {
    let values = Object.assign({}, this.get())
    return values
  }

  return Trace
}
