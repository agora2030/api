'use strict'

module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('Tag', {

    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },

    text: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      index: true
    }

  }, {
    tableName: 'tags',
    paranoid: false,
    hooks: {
      beforeCreate: function (tag) {
        tag.text = tag.text.toLowerCase()
      }
    }
  })

  Tag.associate = (models) => {
    Tag.belongsToMany(models.Initiative, {as: 'initiatives', through: models.InitiativeTags, foreignKey: 'tagId'})
  }

  Tag.initiativeTagsCreate = function (initiative, tags) {
    return new Promise((resolve, reject) => {
      let createTagsPromisses = tags.map(t => {
        let tag = {text: t.toLowerCase()}
        let options = {where: {text: tag.text}, defaults: tag}
        return Tag.findOrCreate(options)
      })
      Promise.all(createTagsPromisses)
      .then(tagsCreated => {
        let tagsIds = tagsCreated.map(t => t[0].id)
        resolve(initiative.setTags(tagsIds))
      }).catch(err => {
        reject(err)
      })
    })
  }

  return Tag
}
