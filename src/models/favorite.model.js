'use strict'

module.exports = (sequelize, DataTypes) => {
  const Favorite = sequelize.define('Favorite', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    }
  }, {
    tableName: 'favorites',
    paranoid: false
  })

  Favorite.associate = (models) => {
    Favorite.belongsTo(models.User, {as: 'user', foreignKey: {field: 'userId', unique: 'unique_favorite', allowNull: false}})
    Favorite.belongsTo(models.Initiative, {as: 'initiative', foreignKey: {field: 'initiativeId', unique: 'unique_favorite', allowNull: false}})
  }

  return Favorite
}
