/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')
const models = require('../../../models')

const ENDPOINT = '/v1/ods'

describe(`#ODS`, () => {
  let userToken = null
  let adminToken = null
  let id = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getToken(server, 'admin')
      .then((adminContext) => {
        adminToken = adminContext.token
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT}`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.exist()
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT}/{id}`, () => {
    it('get one', (done) => {
      models.Ods.create({
        number: 18,
        title: 'CUIDADOS AO DESENVOLVEDOR',
        description: 'Assegurar o café de cada dia',
        color: '#DDA63A'
      })
      .then(ods => {
        const options = {
          method: 'GET',
          url: `${ENDPOINT}/${ods.id}`,
          headers: {'Authorization': `Bearer ${userToken}`}
        }
        server.inject(options, (response) => {
          expect(response.statusCode).to.equals(200)
          expect(response.result).to.exist()
          expect(response.result.id).to.equals(ods.id)
          done()
        })
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })
})
