'use strict'

const Handler = require('./ods.handler')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/ods',
    config: {
      description: 'GET ods',
      notes: 'Get all ods',
      tags: ['api', 'ods'],
      auth: false,
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'GET',
    path: '/ods/{id}',
    config: {
      description: 'GET one ods',
      notes: 'Get one ods by id',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.read,
      validate: {
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  }
]
