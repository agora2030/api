'use strict'

const _ = require('underscore')
const models = require('../../models')

module.exports = {
  list, create, read, update, availables
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      order: [['order', 'ASC']]
    }
    const result = await models.Question.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function create (request, reply) {
  try {
    const result = await models.Question.create(request.payload)
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const result = await models.Question.findById(request.params.id)
    if (!result) {
      return reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const id = request.params.id
    const result = await models.Question.update(request.payload, {where: {id}})
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.Question.findById(id)
    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function availables (request, reply) {
  try {
    const optAnswer = {
      where: {
        initiativeId: request.params.initiativeId
      },
      attributes: ['id'],
      include: [{
        model: models.Question,
        as: 'question'
      }]
    }

    const optQuestion = {
      where: {active: true},
      attributes: ['id', 'title', 'help', 'order', 'mobile']
    }

    const answers = await models.Answer.findAll(optAnswer)
    const questions = await models.Question.findAll(optQuestion)

    const questionsIds = questions.map(item => item.id)
    const answeredQuestionsIds = answers.map(item => item.question.id)

    const questionsAvailablesIds = _.difference(questionsIds, answeredQuestionsIds)

    const questionsAvaliables = _.filter(questions, (q) => {
      return questionsAvailablesIds.indexOf(q.id) !== -1
    })

    const response = {
      total: questionsAvaliables.length,
      questions: questionsAvaliables
    }

    return reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
