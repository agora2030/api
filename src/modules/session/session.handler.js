'use strict'

const models = require('../../models')
const config = require('../../../config/env')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const notification = require('../../notifications/mail')
const rp = require('request-promise')

module.exports = {
  signup,
  login,
  socialSignup,
  socialLogin,
  confirmationSignup,
  solRecoveryPassword,
  recoveryPassword
}

async function signup (request, reply) {
  try {
    request.payload.emailConfirmationToken = crypto.randomBytes(64).toString('hex')
    request.payload.emailConfirmationSendAt = new Date()

    const existsEmail = await models.User.findOne({where: {email: request.payload.email}})

    if (existsEmail) {
      return reply.badData('e-mail is already in use.')
    }

    const user = await models.User.create(request.payload)

    const options = {
      template: 'EmailConfirmacao',
      data: {
        nome: user.firstName,
        email: user.email,
        link_confirmacao: `${config.host}/accountconfirmation?token=${user.emailConfirmationToken}`
      }
    }

    if (config.env !== 'test') {
      notification.sendMail(options)
      .then(sucess => {})
      .catch((err) => { console.log('Error: send email confirmation: ', err) })
    }

    reply({id: user.id})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function login (request, reply) {
  try {
    const {email, password} = request.payload
    const user = await models.User.findOne({where: {email: email}})

    if (!user) {
      return reply.unauthorized('Email or Password invalid')
    }

    if (!user.password) {
      return reply.unauthorized('Email or Password invalid')
    }

    if (!user.validatePassword(password)) {
      return reply.unauthorized('Email or Password invalid')
    }

    if (user.emailConfirmationToken) {
      return reply.badData('Email not confirmed')
    }

    const token = generateToken(user)

    let userData = user.toJSON()
    userData.token = token

    reply(userData)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function socialSignup (request, reply) {
  try {
    let data = request.payload

    const existsEmail = await models.User.findOne({where: {email: data.email}})

    if (existsEmail) {
      return reply.badData('e-mail is already in use.')
    }

    data.socialLogin = true
    data.socialLoginOrigins = [data.socialLoginOrigin]

    const user = await models.User.create(data)

    const options = {
      template: 'EmailBoasVindas',
      data: {
        nome: user.firstName,
        email: user.email
      }
    }

    if (config.env !== 'test') {
      notification.sendMail(options)
      .then(sucess => {})
      .catch((err) => { console.log('Error: send email social confirmation: ', err) })
    }

    let userData = user.toJSON()
    userData.token = generateToken(user)
    reply(userData)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function socialLogin (request, reply) {
  try {
    const origin = request.payload.socialLoginOrigin
    const originUrl = (origin === 'facebook') ? config.facebookUrlCheckToken : config.googleUrlCheckToken
    const checkTokenUrl = (origin === 'google')
      ? `${originUrl}?access_token=${request.payload.accessToken}`
      : `${originUrl}?input_token=${request.payload.accessToken}&access_token=${config.facebookAppToken}`

    const options = {
      uri: checkTokenUrl,
      json: true
    }

    const checkTokenResult = await rp(options)

    if (origin === 'facebook') {
      if (!checkTokenResult.data.is_valid) {
        return reply.badData('Invalid facebook token')
      }
    }

    const email = (origin === 'google') ? checkTokenResult.email : request.payload.email

    const user = await models.User.findOne({where: {email}})

    let result = {}

    if (!user) {
      result.newUser = true
      result.data = null
      return reply(result)
    }

    let userData = user.toJSON()
    userData.token = generateToken(user)

    result.newUser = false
    result.data = userData

    if (request.payload.socialAvatar) {
      await user.update({socialAvatar: request.payload.socialAvatar})
    }

    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function confirmationSignup (request, reply) {
  try {
    const user = await models.User.findOne({where: {emailConfirmationToken: request.params.token}})

    if (!user) {
      return reply.notFound('Token not found')
    }

    await user.update({
      emailConfirmedAt: new Date(),
      emailConfirmationSendAt: null,
      emailConfirmationToken: null
    })

    const options = {
      template: 'EmailBoasVindas',
      data: {
        nome: user.firstName,
        email: user.email
      }
    }

    if (config.env !== 'test') {
      notification.sendMail(options)
      .then(sucess => {})
      .catch((err) => { console.log('Error: send welcome email: ', err) })
    }

    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function solRecoveryPassword (request, reply) {
  try {
    const user = await models.User.findOne({where: {email: request.payload.email}})

    if (!user) {
      return reply.notFound('Email not found')
    }

    const token = crypto.randomBytes(64).toString('hex')

    await user.update({
      solRecoveryPasswordAt: new Date(),
      recoveryPasswordToken: token
    })

    const options = {
      template: 'RecuperarSenha',
      data: {
        nome: user.firstName,
        email: user.email,
        link_recuperar_senha: `${config.host}/recuperar-senha?token=${token}`
      }
    }

    if (config.env !== 'test') {
      notification.sendMail(options)
      .then(sucess => {})
      .catch((err) => { console.log('Error: send recovery password: ', err) })
    }

    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function recoveryPassword (request, reply) {
  try {
    const user = await models.User.findOne({where: {recoveryPasswordToken: request.params.token}})
    if (!user) {
      return reply.notFound('Token not found')
    }
    await user.update({
      recoveryPasswordToken: null,
      solRecoveryPasswordAt: null,
      recoveryPasswordAt: new Date(),
      password: request.payload.password
    })
    reply({message: 'OK'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

function generateToken (user) {
  const secretKey = config.jwtSecret
  return jwt.sign({
    id: user.id,
    scope: [user.role]
  }, secretKey, {expiresIn: '30d'})
}
