'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')

const schema = {
  text: Joi.string(),
  questionId: Helpers.Joi.validate.uuidv4
}

const create = Joi.object({
  text: schema.text.required(),
  questionId: schema.questionId.concat(Joi.required())
})

const update = Joi.object({
  text: schema.text.optional(),
  questionId: schema.questionId.forbidden()
})

const query = Helpers.Joi.validate.pagination.query.concat(
  Joi.object({
    mobile: Joi.boolean()
  })
)

module.exports = {
  create, update, query
}
