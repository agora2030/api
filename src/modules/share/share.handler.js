'use strict'

const models = require('../../models')
const trace = require('../../services/trace')

module.exports = {
  create
}

async function create (request, reply) {
  try {
    const userId = request.auth.credentials.id
    const initiativeId = request.params.initiativeId

    const initiative = await models.Initiative.findById(initiativeId)

    if (!initiative) {
      return reply.notFound()
    }

    trace({
      type: 'actionShare',
      userId: userId,
      initiativeId: initiativeId
    })

    reply({message: 'Ok'})
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
