'use strict'

const Handler = require('./share.handler')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/share',
    config: {
      description: 'Save user share',
      tags: ['api', 'share'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.create,
      validate: {
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  }
]
