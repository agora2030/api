'use strict'

const Joi = require('joi')

const schema = {
  value: Joi.number().integer().min(5).max(50)
}

const create = Joi.object({
  value: schema.value.required()
})

module.exports = {
  create
}
