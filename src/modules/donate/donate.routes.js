'use strict'

const Handler = require('./donate.handler')
const Schema = require('./donate.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/donates',
    config: {
      description: 'GET donates',
      notes: 'Get all donates',
      tags: ['api', 'donates'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/donates',
    config: {
      description: 'donate coins',
      tags: ['api', 'donates'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  }
]
