'use strict'

const Handler = require('./initiative.handler')
const Schema = require('./initiative.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/initiatives',
    config: {
      description: 'GET initiatives',
      notes: 'Get all initiatives',
      tags: ['api', 'initiatives'],
      auth: false,
      handler: Handler.list,
      validate: {
        query: Schema.openQuery
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/agora2030',
    config: {
      description: 'GET initiatives',
      notes: 'Get all initiatives',
      tags: ['api', 'initiatives'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.listAgora,
      validate: {
        query: Schema.query
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives',
    config: {
      description: 'create one initiatives',
      tags: ['api', 'initiatives'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/{slug}',
    config: {
      description: 'GET one initiatives',
      notes: 'Get one initiatives by id',
      tags: ['api', 'user'],
      auth: false,
      handler: Handler.read,
      validate: {
        params: {slug: Schema.slug}
      }
    }
  },
  {
    method: 'GET',
    path: '/initiatives/agora2030/{id}',
    config: {
      description: 'GET one initiatives',
      notes: 'Get one initiatives by id',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.readAgora,
      validate: {
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'PATCH',
    path: '/initiatives/{id}',
    config: {
      description: 'Update one initiatives',
      tags: ['api', 'initiatives'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.update,
      validate: {
        payload: Schema.update,
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/tags',
    config: {
      description: 'Add one tag on initiative',
      tags: ['api', 'initiatives', 'tags'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.addTag,
      validate: {
        payload: Schema.addTag,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'DELETE',
    path: '/initiatives/{initiativeId}/tags/{id}',
    config: {
      description: 'Remove one tag on initiative',
      tags: ['api', 'initiatives', 'tags'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.removeTag,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/ods',
    config: {
      description: 'Add one ods on initiative',
      tags: ['api', 'initiatives', 'ods'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.addOds,
      validate: {
        payload: Schema.addOds,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'DELETE',
    path: '/initiatives/{initiativeId}/ods/{id}',
    config: {
      description: 'Remove one ods on initiative',
      tags: ['api', 'initiatives', 'ods'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.removeOds,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{id}/publish',
    config: {
      description: 'Publish and unpublish solution',
      tags: ['api', 'initiatives'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.updatePublishStatus,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4
        },
        payload: Schema.updatePublishStatus
      }
    }
  }
]
