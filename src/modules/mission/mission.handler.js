'use strict'

const models = require('../../models')

module.exports = {
  list, create, read, update
}

async function list (request, reply) {
  try {
    const options = {
      limit: request.query.limit,
      offset: request.query.skip,
      order: [['order', 'ASC']]
    }
    const result = await models.Mission.findAndCountAll(options)
    const response = {
      total: result.count,
      skip: request.query.skip,
      limit: request.query.limit,
      items: result.rows
    }
    reply(response)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function create (request, reply) {
  try {
    const result = await models.Mission.create(request.payload)
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function read (request, reply) {
  try {
    const result = await models.Mission.findById(request.params.id)
    if (!result) {
      return reply.notFound()
    }
    reply(result)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}

async function update (request, reply) {
  try {
    const id = request.params.id
    const result = await models.Mission.update(request.payload, {where: {id}})
    if (result[0] === 0) {
      return reply.notFound()
    }
    const updated = await models.Mission.findById(id)
    reply(updated)
  } catch (error) {
    reply.badImplementationCustom(error)
  }
}
