'use strict'

const Handler = require('./mission.handler')
const Schema = require('./mission.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/missions',
    config: {
      description: 'GET missions',
      notes: 'Get all missions',
      tags: ['api', 'missions'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/missions',
    config: {
      description: 'create missions',
      tags: ['api', 'missions'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create
      }
    }
  },
  {
    method: 'GET',
    path: '/missions/{id}',
    config: {
      description: 'GET one mission',
      notes: 'Get one missions by id',
      tags: ['api', 'user'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.read,
      validate: {
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'PATCH',
    path: '/missions/{id}',
    config: {
      description: 'Update one mission',
      tags: ['api', 'missions'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.update,
      validate: {
        payload: Schema.update,
        params: {id: Helpers.Joi.validate.uuidv4}
      }
    }
  }
]
