/* global describe, before, it, expect, server */

const utils = require('../../../test/utils')
const ENDPOINT = '/v1/missions'

describe(`#Mission`, () => {
  let userToken = null
  let adminToken = null
  let id = null

  before((done) => {
    utils.getToken(server)
    .then((userContext) => {
      userToken = userContext.token
      utils.getToken(server, 'admin')
      .then((adminContext) => {
        adminToken = adminContext.token
        done()
      })
    })
  })

  describe(`POST ${ENDPOINT}`, () => {
    it('create a new', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          title: 'Povoado',
          description: 'O mapa precisa de pessoas',
          order: 6,
          actionLike: 2,
          actionView: 4,
          actionShare: 4
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.exist()
        id = response.result.id
        done()
      })
    })

    it('403 - role', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${userToken}`},
        payload: {
          title: 'Povoado',
          description: 'O mapa precisa de pessoas',
          order: 6,
          actionLike: 2,
          actionView: 4,
          actionShare: 4
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(403)
        done()
      })
    })

    it('not send required data', (done) => {
      const options = {
        method: 'POST',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT}`, () => {
    it('get all', (done) => {
      const options = {
        method: 'GET',
        url: ENDPOINT,
        headers: {'Authorization': `Bearer ${adminToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.total).to.greaterThan(0)
        expect(response.result.skip).to.exist()
        expect(response.result.limit).to.exist()
        expect(response.result.items).to.not.be.empty()
        done()
      })
    })
  })

  describe(`GET ${ENDPOINT}/{id}`, () => {
    it('get one', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        done()
      })
    })

    it('not found', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/09919733-3d2f-4d7a-a9bd-ad6d4c3822e7`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(404)
        done()
      })
    })

    it('invalid id', (done) => {
      const options = {
        method: 'GET',
        url: `${ENDPOINT}/invalid`,
        headers: {'Authorization': `Bearer ${userToken}`}
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(400)
        done()
      })
    })
  })

  describe(`PATCH ${ENDPOINT}/{id}`, () => {
    it('update one', (done) => {
      const options = {
        method: 'PATCH',
        url: `${ENDPOINT}/${id}`,
        headers: {'Authorization': `Bearer ${adminToken}`},
        payload: {
          title: 'Title updated'
        }
      }
      server.inject(options, (response) => {
        expect(response.statusCode).to.equals(200)
        expect(response.result).to.exist()
        expect(response.result.id).to.equals(id)
        expect(response.result.title).to.equals('Title updated')
        done()
      })
    })
  })
})
