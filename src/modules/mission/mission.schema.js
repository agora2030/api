'use strict'

const Joi = require('joi')

const schema = {
  title: Joi.string().max(100),
  description: Joi.string().max(700),
  order: Joi.number().integer(),
  actionLike: Joi.number().integer(),
  actionView: Joi.number().integer(),
  actionShare: Joi.number().integer(),
  actionDonate: Joi.number().integer(),
  actionFavorite: Joi.number().integer()
}

const create = Joi.object({
  title: schema.title.required(),
  description: schema.description.required(),
  order: schema.order.required(),
  actionLike: schema.actionLike.optional(),
  actionView: schema.actionView.optional(),
  actionShare: schema.actionShare.optional(),
  actionDonate: schema.actionDonate.optional(),
  actionFavorite: schema.actionFavorite.optional()
}).required().min(5)

const update = Joi.object({
  title: schema.title.optional(),
  description: schema.description.optional(),
  order: schema.order.optional(),
  actionLike: schema.actionLike.optional(),
  actionView: schema.actionView.optional(),
  actionShare: schema.actionShare.optional(),
  actionDonate: schema.actionDonate.optional(),
  actionFavorite: schema.actionFavorite.optional()
}).required().min(1)

module.exports = {
  create, update
}
