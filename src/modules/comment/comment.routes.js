'use strict'

const Handler = require('./comment.handler')
const Schema = require('./comment.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/comments',
    config: {
      description: 'GET comments',
      notes: 'Get all comments',
      tags: ['api', 'comments'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/comments',
    config: {
      description: 'create comment',
      tags: ['api', 'comments'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'PATCH',
    path: '/initiatives/{initiativeId}/comments/{id}',
    config: {
      description: 'Update one comment',
      notes: 'Update one comment',
      tags: ['api', 'comments'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.update,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        },
        payload: Schema.update
      }
    }
  },
  {
    method: 'DELETE',
    path: '/initiatives/{initiativeId}/comments/{id}',
    config: {
      description: 'Delete one comment',
      notes: 'Delete one comment',
      tags: ['api', 'comments'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.destroy,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  }
]
