
const models = require('../../models')
const moment = require('moment')
const xlr = require('xlr')
const path = require('path')

module.exports = {
  usersReport
}

async function usersReport (request, reply) {
  const rows = await getUsersData()
  let data = []
  let columsConfig = [
    {value: 'Nome', style: 1},
    {value: 'Email', style: 1},
    {value: 'XP', style: 1},
    {value: 'Nível', style: 1},
    {value: 'Missão atual', style: 1},
    {value: 'Última atividade', style: 1},
    {value: 'Total. Ações', style: 1},
    {value: 'Views', style: 1},
    {value: 'Likes', style: 1},
    {value: 'Compartilhamentos', style: 1},
    {value: 'Favoritos', style: 1},
    {value: 'Apoios', style: 1},
    {value: 'Comentários', style: 1},
    {value: 'País', style: 1},
    {value: 'Estado', style: 1},
    {value: 'Cidade', style: 1},
    {value: 'Telefone', style: 1},
    {value: 'Formação', style: 1},
    {value: 'Ocupação', style: 1},
    {value: 'Gênero', style: 1}
  ]
  data.push(columsConfig)
  for (var i = 0; i < rows.length; i++) {
    let element = [
      `${rows[i].firstName} ${rows[i].lastName}` || 'Não informado',
      rows[i].email || 'Não informado',
      rows[i].xp || '0',
      getUserNivelName(rows[i].nivel),
      rows[i].actualMission,
      (rows[i].lastActivity) ? moment(rows[i].lastActivity).format('DD/MM/YYYY - h:mm:ss a') : 'Sem rastro',
      rows[i].numberOfActions,
      rows[i].trace.actionView,
      rows[i].trace.actionLike,
      rows[i].trace.actionShare,
      rows[i].trace.actionFavorite,
      rows[i].trace.actionDonate,
      rows[i].trace.actionComment,
      rows[i].country || 'Não informado',
      rows[i].state || 'Não informado',
      rows[i].city || 'Não informado',
      rows[i].phoneNumber || 'Não informado',
      rows[i].major || 'Não informado',
      rows[i].occupation || 'Não informado',
      rows[i].gender || 'Não informado'
    ]
    data.push(element)
  }
  const conf = {
    stylesXmlFile: path.join(__dirname, '../../../config/styles.xml'),
    name: 'usuarios',
    columns: [
      {type: 'string', caption: 'Nome', width: 30},
      {type: 'string', caption: 'Email', width: 30},
      {type: 'string', caption: 'XP', width: 10},
      {type: 'string', caption: 'Nível', width: 20},
      {type: 'string', caption: 'Missão atual', width: 20},
      {type: 'string', caption: 'Última atividade', width: 25},
      {type: 'string', caption: 'Total. Ações', width: 15},
      {type: 'string', caption: 'Views', width: 15},
      {type: 'string', caption: 'Likes', width: 15},
      {type: 'string', caption: 'Compartilhamentos', width: 15},
      {type: 'string', caption: 'Favoritos', width: 15},
      {type: 'string', caption: 'Apoios', width: 15},
      {type: 'string', caption: 'Comentários', width: 15},
      {type: 'string', caption: 'País', width: 20},
      {type: 'string', caption: 'Estado', width: 20},
      {type: 'string', caption: 'Cidade', width: 20},
      {type: 'string', caption: 'Telefone', width: 20},
      {type: 'string', caption: 'Formação', width: 20},
      {type: 'string', caption: 'Ocupação', width: 20},
      {type: 'string', caption: 'Gênero', width: 20}
    ],
    rows: data
  }

  const fileBuffer = new Buffer(xlr(conf), 'binary')

  return reply(fileBuffer)
    .header('Content-disposition', 'attachment; filename=usuarios.xlsx')
    .header('Content-type', 'application/vnd.openxmlformats')
    .header('Content-length', fileBuffer.length).encoding('binary')
}

async function getUsersData () {
  try {
    const usersStats = []
    const users = await models.User.findAll({
      order: [
        ['trace', 'createdAt', 'DESC'],
        ['missions', 'order', 'DESC']
      ],
      include: [
        {
          models: models.Trace,
          association: 'trace',
          required: false
        },
        {
          attributes: ['title', 'order'],
          association: 'missions',
          through: {attributes: []},
          required: false
        }
      ]
    })

    for (let i = 0; i < users.length; i++) {
      let user = users[i].toJSON()
      let actTotal = {
        actionLike: 0,
        actionView: 0,
        actionShare: 0,
        actionDonate: 0,
        actionFavorite: 0,
        actionComment: 0
      }

      user.numberOfActions = user.trace.length
      user.lastActivity = (user.trace[0]) ? user.trace[0].createdAt : null
      user.actualMission = (user.missions[0]) ? user.missions[0].title : 'Sem missão'

      for (let t = 0; t < user.trace.length; t++) {
        actTotal[user.trace[t].type]++
      }

      user.trace = actTotal

      delete user.missions

      usersStats.push(user)
    }

    return usersStats
  } catch (error) {
    console.log(error)
  }
}

function getUserNivelName (nivel) {
  switch (nivel) {
    case 1:
      return 'Nível Acampamento'
    case 2:
      return 'Nível Casa'
    case 3:
      return 'Nível Comunidade'
    case 4:
      return 'Nível País'
    case 5:
      return 'Nível mundo'
    default:
      return 'Nível Acampamento'
  }
}
