'use strict'

const Handler = require('./url.handler')
const Schema = require('./url.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/initiatives/{initiativeId}/urls',
    config: {
      description: 'GET urls',
      notes: 'Get all urls',
      tags: ['api', 'urls'],
      auth: false,
      handler: Handler.list,
      validate: {
        query: Schema.query,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/initiatives/{initiativeId}/urls',
    config: {
      description: 'create url',
      tags: ['api', 'urls'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create,
        params: {initiativeId: Helpers.Joi.validate.uuidv4}
      }
    }
  },
  {
    method: 'DELETE',
    path: '/initiatives/{initiativeId}/urls/{id}',
    config: {
      description: 'Delete one url',
      notes: 'Delete one url',
      tags: ['api', 'urls'],
      auth: {
        scope: ['admin']
      },
      handler: Handler.destroy,
      validate: {
        params: {
          id: Helpers.Joi.validate.uuidv4,
          initiativeId: Helpers.Joi.validate.uuidv4
        }
      }
    }
  }
]
