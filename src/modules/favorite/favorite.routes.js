'use strict'

const Handler = require('./favorite.handler')
const Schema = require('./favorite.schema')
const Helpers = require('../../core/lib/helpers')

module.exports = [
  {
    method: 'GET',
    path: '/profile/favorites',
    config: {
      description: 'GET favorites',
      notes: 'Get all favorites',
      tags: ['api', 'profile', 'favorites'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.list,
      validate: {
        query: Helpers.Joi.validate.pagination.query
      },
      response: {
        schema: Helpers.Joi.validate.pagination.response
      }
    }
  },
  {
    method: 'POST',
    path: '/profile/favorites',
    config: {
      description: 'create favorite',
      tags: ['api', 'profile', 'favorites'],
      auth: {
        scope: ['admin', 'user']
      },
      handler: Handler.create,
      validate: {
        payload: Schema.create
      }
    }
  }
]
