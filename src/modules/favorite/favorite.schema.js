'use strict'

const Joi = require('joi')
const Helpers = require('../../core/lib/helpers')

const schema = {
  initiativeId: Helpers.Joi.validate.uuidv4
}

const create = Joi.object({
  initiativeId: schema.initiativeId.concat(Joi.required())
})

module.exports = {
  create
}
