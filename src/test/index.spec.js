'use strict'

const Lab = require('lab')
const Code = require('code')
const bootstrap = require('../core/bootstrap')
const models = require('../models')
const lab = exports.lab = Lab.script()

global.expect = Code.expect

global.it = lab.it
global.before = lab.before
global.beforeEach = lab.beforeEach
global.after = lab.after
global.describe = lab.describe

global.describe('#Load server', () => {
  global.before((done) => {
    bootstrap.start().then((server) => {
      console.log('--> Server executed')
      global.server = server
      global.models = models
      done()
    })
    .catch((err) => {
      console.log('Error loading bootstrap')
      console.log(err)
    })
  })

  global.it('load server finalized', (done) => {
    global.expect(global.server).to.exist()
    global.expect(global.models).to.exist()
    done()
  })
})
