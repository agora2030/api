'use strict'

const models = require('../models')
const Helpers = require('../core/lib/helpers')
const request = require('request-promise')
const config = require('../../config/env')
const notification = require('../notifications/mail')
const crypto = require('../core/lib/crypto')
const moment = require('moment')

module.exports = addTrace

async function addTrace (options) {
  try {
    const findOptions = {where: options, defaults: options}

    const trace = await models.Trace.findOrCreate(findOptions)

    if (trace[1]) { // check if is a new register
      const user = await models.User.findById(options.userId)
      const newUserXp = actionPoints(options.type) + user.xp
      const newLevel = Helpers.userLevel(newUserXp)

      const updates = {
        xp: newUserXp
      }

      if (newLevel > user.userLevel()) {
        // Level Up!
        updates.coins = user.coins + bonusCoinsByLevel(newLevel)

        if (newLevel === 4) {
          // bonus by level
          updates.favorites = user.favorites + 5
        }
        let levelUpNotify = {
          title: `Você atingiu o ${getUserNivelName(newLevel)}`,
          msg: pushNivelMsg(newLevel),
          userId: options.userId
        }
        notifyPush(levelUpNotify)
      }

      const completedMission = await hasSomeCompletedMission(options.userId)

      if (completedMission.completed) {
        let missionCompletedNotifyPush = {
          userId: options.userId
        }

        let notifyOptsEmail = {
          missionTitle: completedMission.missions[0].title,
          data: {
            email: user.email
          }
        }

        if (completedMission.missions[0].title === 'Curadoria') {
          moment.locale('pt-br')
          const dateFomated = moment(completedMission.missions[0].createdAt).format('LL')
          const hash = crypto.encrypt(JSON.stringify({
            dateFomated,
            name: `${user.firstName} ${user.lastName}`
          }))
          const urlCertificate = `${config.apiUrl}/user/certificate?data=${hash}`
          notifyOptsEmail.data.urlCertificate = urlCertificate
          missionCompletedNotifyPush.title = `Parabéns agora você é um Curador!`
          missionCompletedNotifyPush.msg = `Vá ao início e veja a evolução do povoado ágora. O seu certificado será envia por e-mail! =)`
        } else {
          missionCompletedNotifyPush.title = `Você completou a missão ${completedMission.missions[0].title}!`
          missionCompletedNotifyPush.msg = `Vá ao início e veja a evolução do povoado ágora. Toque no item da missão e veja o selo que você desbloqueou.`
        }

        notifyPush(missionCompletedNotifyPush)
        notifyEmail(notifyOptsEmail)
      }

      await user.update(updates)
    }
  } catch (error) {
    console.log('Error on set Trace: ', error)
  }
}

function notifyEmail (options) {
  const optionsNot = {
    template: `Missao${options.missionTitle}`,
    data: options.data
  }

  if (config.env !== 'test') {
    notification.sendMail(optionsNot)
    .then(sucess => {})
    .catch((err) => { console.log('Error: send email mission: ', err) })
  }
}

function notifyPush (options) {
  if (config.env === 'test') {
    return
  }
  const sendOpt = {
    method: 'POST',
    uri: 'https://onesignal.com/api/v1/notifications',
    body: {
      app_id: config.oneSignalAppId,
      headings: {'en': options.title},
      contents: {'en': options.msg},
      filters: [{'field': 'tag', 'key': 'id', 'value': options.userId}]
    },
    headers: {
      Authorization: `Basic ${config.oneSignalToken}`
    },
    json: true
  }
  request(sendOpt)
  .then((body) => {
    // POST succeeded...
  })
  .catch((err) => {
    console.log('Erro ao notificar usuário: ', err, options)
  })
}

async function hasSomeCompletedMission (userId) {
  try {
    let result = {
      completed: false,
      missions: []
    }
    const userMissions = await models.UserMissions.findAndCountAll({
      where: {userId}
    })

    if (userMissions.count >= 6) {
      // não existe mais missões, pode parar de checar
      return result
    }

    const actTotal = {
      actionLike: 0,
      actionView: 0,
      actionShare: 0,
      actionDonate: 0,
      actionFavorite: 0,
      actionComment: 0
    }

    const userActions = await models.Trace.findAll({
      attributes: ['type'],
      where: {userId}
    })

    for (let i = 0; i < userActions.length; i++) {
      let action = userActions[i]
      actTotal[action.type]++
    }

    const missions = await models.Mission.findAndCountAll()
    const userMissionsIds = userMissions.rows.map(item => item.missionId)

    let newMissionsCompleted = []

    for (let i = 0; i < missions.rows.length; i++) {
      let mission = missions.rows[i].toJSON()
      let completed = [
        (actTotal.actionLike >= mission.actionLike),
        (actTotal.actionView >= mission.actionView),
        (actTotal.actionShare >= mission.actionShare),
        (actTotal.actionDonate >= mission.actionDonate),
        (actTotal.actionFavorite >= mission.actionFavorite),
        (actTotal.actionComment >= mission.actionComment)
      ].every(item => item === true)
      if (completed && userMissionsIds.indexOf(mission.id) === -1) {
        result.missions.push(mission)
        newMissionsCompleted.push({
          missionId: mission.id,
          userId: userId
        })
      }
    }

    if (newMissionsCompleted.length === 0) {
      return result
    }

    await models.UserMissions.bulkCreate(newMissionsCompleted, {individualHooks: true})

    result.completed = true

    return result
  } catch (error) {
    console.log(`Error on verify completed missions to user: ${userId} - Error: ${error}`)
  }
}

function actionPoints (action) {
  switch (action) {
    case 'actionFavorite':
      return 20
    case 'actionLike':
      return 10
    case 'actionDonate':
      return 10
    case 'actionComment':
      return 5
    case 'actionShare':
      return 15
    default:
      return 0
  }
}

function bonusCoinsByLevel (level) {
  switch (level) {
    case 2:
      return 10
    case 3:
      return 15
    case 4:
      return 75
    default:
      return 0
  }
}

function getUserNivelName (nivel) {
  switch (nivel) {
    case 1:
      return 'Nível Acampamento'
    case 2:
      return 'Nível Casa'
    case 3:
      return 'Nível Comunidade'
    case 4:
      return 'Nível País'
    case 5:
      return 'Nível mundo'
    default:
      return 'Nível Acampamento'
  }
}

function pushNivelMsg (nivel) {
  switch (nivel) {
    case 2:
      return 'Como recompensa você ganhou 10 seeds. Parabéns! =)'
    case 3:
      return 'Como recompensa você ganhou 15 seeds. Parabéns! =)'
    case 4:
      return `Como recompensa você ganhou mais 5 favoritos, 75 seeds e agora também pode apoiar as iniciativas. Parabéns! =)`
    case 5:
      return 'Wooow! Você atingiu o nível máximo do desafio. Parabéns pela conquista! =)'
    default:
      return `Continue a jogar e ganhe mais pontos para subir de nível. =)`
  }
}
