var Joi = require('joi')

exports.regex = {
  uuidv4: /([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/
}

exports.Joi = {
  validate: {
    uuidv4: Joi.string().regex(exports.regex.uuidv4),
    pagination: {
      response: Joi.object({
        total: Joi.number().integer(),
        skip: Joi.number().integer(),
        limit: Joi.number().integer(),
        items: Joi.array().items(Joi.object())
      }),
      query: Joi.object({
        skip: Joi.number().integer().default(0),
        limit: Joi.number().integer().min(1).max(50).default(10)
      })
    }
  }
}

exports.userLevel = (xp) => {
  switch (true) {
    case (xp >= 0 && xp < 50):
      return 1
    case (xp >= 50 && xp < 150):
      return 2
    case (xp >= 150 && xp < 300):
      return 3
    case (xp >= 300 && xp < 900):
      return 4
    case (xp >= 900):
      return 5
    default:
      return 1
  }
}

exports.userXpRange = (level) => {
  switch (level) {
    case 1:
      return [0, 49]
    case 2:
      return [50, 149]
    case 3:
      return [150, 299]
    case 4:
      return [300, 899]
    case 5:
      return [900, 5000]
    default:
      return [0, 49]
  }
}
