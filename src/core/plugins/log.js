'use strict'

const Good = require('good')

exports.register = (server, options, next) => {
  const opt = {
    ops: {
      interval: 1000
    },
    reporters: {
      console: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{ log: '*', error: '*', request: '*', response: '*' }]
      }, {
        module: 'good-console'
      }, 'stdout'],
      file: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{ ops: '*', error: '*' }]
      }, {
        module: 'good-squeeze',
        name: 'SafeJson'
      }]
    }
  }

  server.register({
    register: Good,
    options: process.env.NODE_ENV === 'test' ? {} : opt
  }, (err) => {
    return next(err)
  })
}

exports.register.attributes = {
  name: 'logs',
  version: '1.0.0'
}
