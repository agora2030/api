'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'urls', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        url: {
          type: Sequelize.STRING,
          allowNull: false
        },

        title: {
          type: Sequelize.STRING
        },

        type: {
          type: Sequelize.ENUM('audio', 'video', 'link')
        },

        initiativeId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('urls')
  }
}
