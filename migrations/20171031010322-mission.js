'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'missions', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        title: {
          type: Sequelize.STRING,
          allowNull: false
        },

        description: {
          type: Sequelize.TEXT('long'),
          allowNull: false
        },

        order: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        actionLike: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        actionView: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        actionShare: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        actionDonate: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        actionFavorite: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        actionComment: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('missions')
  }
}
