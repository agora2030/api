'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'traces', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        type: {
          type: Sequelize.ENUM('actionLike', 'actionView', 'actionShare', 'actionDonate', 'actionFavorite', 'actionComment'),
          allowNull: false
        },

        userId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        initiativeId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('traces')
  }
}
