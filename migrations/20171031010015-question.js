'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'questions', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        title: {
          type: Sequelize.TEXT('medium'),
          allowNull: false
        },

        help: {
          type: Sequelize.TEXT('medium')
        },

        active: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },

        mobile: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },

        order: {
          type: Sequelize.INTEGER
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('questions')
  }
}
