'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'comments', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        text: {
          type: Sequelize.TEXT('long'),
          allowNull: false
        },

        userId: {
          type: Sequelize.UUID,
          allowNull: false
        },

        initiativeId: {
          type: Sequelize.UUID
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('comments')
  }
}
