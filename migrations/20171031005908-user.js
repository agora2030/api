'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(
      'users', {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        role: {
          type: Sequelize.ENUM('admin', 'user'),
          defaultValue: 'user'
        },

        firstName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        lastName: {
          type: Sequelize.STRING,
          allowNull: false
        },

        email: {
          type: Sequelize.STRING,
          unique: true,
          allowNull: false,
          validate: {
            isEmail: true
          }
        },

        xp: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },

        coins: {
          type: Sequelize.INTEGER,
          defaultValue: 100
        },

        favorites: {
          type: Sequelize.INTEGER,
          defaultValue: 10
        },

        password: {
          type: Sequelize.STRING
        },

        country: {
          type: Sequelize.STRING
        },

        state: {
          type: Sequelize.STRING
        },

        city: {
          type: Sequelize.STRING
        },

        gender: {
          type: Sequelize.ENUM('Masculino', 'Feminino')
        },

        major: {
          type: Sequelize.STRING
        },

        occupation: {
          type: Sequelize.STRING
        },

        phoneNumber: {
          type: Sequelize.STRING
        },

        socialLogin: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },

        socialLoginOrigins: {
          type: Sequelize.STRING,
          get: function () {
            return this.getDataValue('socialLoginOrigins') ? this.getDataValue('socialLoginOrigins').split(';') : ''
          },
          set: function (val) {
            this.setDataValue('socialLoginOrigins', val.join(';'))
          }
        },

        socialAvatar: {
          type: Sequelize.STRING
        },

        avatarPath: {
          type: Sequelize.STRING
        },

        avatarExtName: {
          type: Sequelize.STRING
        },

        receiveEmail: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },

        receivePush: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },

        emailConfirmationToken: {
          type: Sequelize.STRING
        },

        emailConfirmationSendAt: {
          type: Sequelize.DATE
        },

        emailConfirmedAt: {
          type: Sequelize.DATE
        },

        recoveryPasswordToken: {
          type: Sequelize.STRING
        },

        solRecoveryPasswordAt: {
          type: Sequelize.DATE
        },

        recoveryPasswordAt: {
          type: Sequelize.DATE
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

        deletedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('users')
  }
}
